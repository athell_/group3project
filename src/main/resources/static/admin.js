let data = [];
let currentSection = null;
let currentProduct = null;
let currentSectionNo = null
const state = {
    sectionOpen: null,
    productOpen: null,
    addSection: null,
    addProduct: null,
    updateSection: null,
    updateProduct: null,
    login: null,

};

function refresh() {
    fetch("./sections")
        .then((Response) => Response.json())
        .then((_data) => {
            data = _data;
            render();
        })
        .catch((err) => console.error(err));
}
refresh()

function render() {
    if (!state.login) {
        let loginContent = `
    <section class="modal-bg" >
            <section class="modal-box-add">
                <section class="box-header"><div>Login</div></section>
                    <section class="additem">
        <article>
            <form class="form" onsubmit="event.preventDefault();adminLogin(this);" >
                <label>Login</label>
                <input name="Login" required autocomplete="off">
                <label>Password</label>
                <input name="Password" required autocomplete="off" type="password"></label>
                    <button style="width:13rem;">Sign In</button>
            </form>
            </article>
            </section></section></section>
    `

        const logonEl = document.getElementById("logon");
        logonEl.innerHTML = loginContent;

    } else {
        const logonEl = document.getElementById("logon");
        logonEl.innerHTML = "";
    }

    let content = `  
    <section class="box-header">SELECT / ADD A SECTION</section>
    <br>
    <section class="modal-grid">
    ${data.map((section, i) => {
        return `
        <article class="modal-item" >
            <div class="box-Image" onclick="displayProducts(${i})" style=" background-image: url(${section.imageURL} );">
            </div>
            <div class="box-link" onclick="displayProducts(${i})">
            <a>${section.name}</a>                    
            </div>
            <div>
                <button style="width: 100%; margin-bottom: 2%" onclick="displayProducts(${i}); getUpdateSectionFormHtml()" >Update Section</button>
                <button style="width: 100%; margin-bottom: 2%" onclick="displayProducts(${i}); deleteSection()">DELETE</button>
            </div>
            </article>`
        }).join("")}
        <article class="modal-item" onclick="getAddSectionFormHtml()">
            <div class="box-Image">
            <div class="box-Image-top">
            
            </div><a> Add Section </a></div>
            <div class="box-link">
                                
            </div>
            </article>                    
        </section>`

    const contentEl = document.getElementById("sectionsSelected");
    contentEl.innerHTML = content;

    if(state.sectionOpen){
        let products = `
        <br>
        <section class="box-header">SELECT / ADD A PRODUCT</section>
        <br>
        <section class="modal-grid">
        ${state.sectionOpen.map((product, i) => {
            return `
            <article class="modal-item" onclick="selectedProduct(${i});getUpdateProductFormHtml()">
                <div class="box-Image" style="background-image: url(${product.imageURL} );">
                </div>
                <div class="box-link">
                <a>${product.name}</a>
                <a>${product.description}</a>
                <a>Price:${product.price}</a>
                <a>Stock: ${product.stockAmount}</a>                    
                </div>
                <div>
                    <button <button style="width: 100%;" onclick="selectedProduct(${i}); deleteProduct()">DELETE</button>
                </div>
                </article>`
            }).join("")}
            <article class="modal-item" onclick="getAddProductFormHtml()">
                <div class="box-Image">
                <div class="box-Image-top">
                
                </div><a> Add Product </a></div>
                <div class="box-link">
                                    
                </div>
                </article>                    
            </section>
        
        
        
        `
        const productsEl = document.getElementById("productsSelected");
        productsEl.innerHTML = products;

    }else{
        const productsEl = document.getElementById("productsSelected");
        productsEl.innerHTML = "";
    }


}


function displayProducts(index){
    currentSectionNo = index
    currentSection = data[index]

    state.sectionOpen = currentSection.products;
    render()
}
function selectedProduct(index){
    currentProduct= currentSection.products[index]
    state.productOpen = currentProduct

}
function getAddSectionFormHtml(){
    state.addSection = true
    if (state.addSection) {
        const modalContentaddSection = 
        `
        <section class="modal-bg" >
            <section class="modal-box-add">
                <section class="box-header"><div>Add Section</div><button style="justify-content: right;" onclick="closeAddSection()">X</button></section>
                    <section class="additem">
        <article>
            <form class="form" onsubmit="event.preventDefault();addSection(this);" >
                <label>Section Name</label>
                <input name="name" required>
                <label>Section image</label>
                <input name="imageURL" type="url" required></label>
                    <button style="width:13rem;">Add Section</button>
            </form>
            </article>
            </section></section></section>
        `
        const modalEladdSection = document.getElementById('addSectionModal')
        modalEladdSection.innerHTML = modalContentaddSection
        
    } else {
        const modalEladdSection = document.getElementById('addSectionModal')
        modalEladdSection.innerHTML = ""
    }
}
function addSection(HTMLform){
    const mapping = `/sections/`
    const data = new FormData(HTMLform)
    const name = data.get('name')
    const imageURL = data.get('imageURL')
    fetch(mapping, {
        method: 'POST',
        headers: {
            "content-Type": "application/json"
        },
        body: JSON.stringify({name, imageURL})
    }).then(res => res.json()).then(console.log).catch(console.error)
    confirm()
 
}


function getAddProductFormHtml(){
    state.addProduct = true
    if (state.addProduct) {
        const modalContentaddProduct = 
        `
        <section class="modal-bg" >
            <section class="modal-box-add">
                <section class="box-header"><div>Add Product</div><button style="justify-content: right;" onclick="closeAddProduct()">X</button></section>
                    <section class="additem">
        <article>
            <form class="form" onsubmit="event.preventDefault();addProduct(this);" >
                <label>Procuct Name</label>
                <input name="name" required>
                <label>Description</label>
                <input name="description" required>
                <label>Price</label>
                <input name="price" required>
                <label>Stock Level</label>
                <input name="stockAmount" required>
                <label>Product image</label>
                <input name="imageURL" type="url" required></label>
                    <button style="width:13rem;">Add Product</button>
            </form>
            </article>
            </section></section></section>
        `
        const modalEladdProduct = document.getElementById('addProductModal')
        modalEladdProduct.innerHTML = modalContentaddProduct
        
    } else {
        const modalEladdProduct = document.getElementById('addProductModal')
        modalEladdProduct.innerHTML = ""
    }
}
function addProduct(HTMLform){
    const mapping = `/sections/${currentSection.id}/products`
    const data = new FormData(HTMLform)
    const name = data.get('name')
    const description = data.get('description')
    const price = data.get('price')
    const stockAmount = data.get('stockAmount')
    const imageURL = data.get('imageURL')
    fetch(mapping, {
        method: 'POST',
        headers: {
            "content-Type": "application/json"
        },
        body: JSON.stringify({name, description, price, stockAmount, imageURL})
    }).then(res => res.json()).then(console.log).catch(console.error)
    confirm()
}
function adminLogin(HTMLform){
    let user = "admin"
    let pass = "_admin"
    let loginData = new  FormData(HTMLform)
    let login = loginData.get('Login')
    const Password = loginData.get('Password')
    if(login == user && Password == pass){
        state.login=true
        const logonEl = document.getElementById("logon");
        logonEl.innerHTML = "";
        render()
    }else{
        state.login = null
        render()
}

}
function closeAddSection(){
    state.addSection = null
    const modalEladdSection = document.getElementById('addSectionModal')
    modalEladdSection.innerHTML = ""
    render()
}
function closeAddProduct(){
    state.addProduct = null
    const modalEladdProduct = document.getElementById('addProductModal')
    modalEladdProduct.innerHTML = ""
    render()
}
function closeUpdateSection(){
    state.updateSection = null
    const modalElUpdateSection = document.getElementById('updateSectionModal')
    modalElUpdateSection.innerHTML = ""
    render()
}
function closeUpdateProduct(){
    state.updateProduct = null
    const modalElUpdateProduct = document.getElementById('updateProductModal')
        modalElUpdateProduct.innerHTML = ""
    render()
}
function confirm(){
    const modalElConfirmButt = 
        `
        <section class="modal-bg" >
            <section class="modal-box-confirm">
                <div style="margin-top: 30%; margin-bottom: 10%;">
                    <div>Update/Delete Complete</div>
                    <button style="justify-content: centre; margin-left: 40%;" onclick="closeConfirm(); closeAddSection(); closeAddProduct() ">OK</button>
                </div>
            </section>
        </section>
        `

    const modalElConfirm = document.getElementById('confirm')
    modalElConfirm.innerHTML = modalElConfirmButt
}
function closeConfirm(){
    state.updateSection = null,
    state.updateProduct= null,
    state.addSection = null
    state.addProduct = null
    state.sectionOpen= null
    state.productOpen= null
    const modalElUpdateSection = document.getElementById('updateSectionModal')
    modalElUpdateSection.innerHTML = ""
    const modalElUpdateProduct = document.getElementById('updateProductModal')
    modalElUpdateProduct.innerHTML = ""
    const modalEladdSection = document.getElementById('addSectionModal')
    modalEladdSection.innerHTML = ""
    const modalEladdProduct = document.getElementById('addProductModal')
    modalEladdProduct.innerHTML = ""   
    const modalElConfirm = document.getElementById('confirm')
    modalElConfirm.innerHTML = ""
    refresh()
    render()
}
function getUpdateProductFormHtml(){
    state.updateProduct = true
    if (state.updateProduct) {
        const modalContentUpdateProduct = 
        `
        <section class="modal-bg" >
            <section class="modal-box-add">
                <section class="box-header"><div>Update Product</div><button style="justify-content: right;" onclick="closeUpdateProduct()">X</button></section>
                    <section class="additem">
        <article>
            <form class="form" onsubmit="event.preventDefault();updateProduct(this);" >
                <label>Procuct Name</label>
                <input name="name" required value="${currentProduct.name}">
                <label>Description</label>
                <input name="description" required value="${currentProduct.description}">
                <label>Price</label>
                <input name="price" required value="${currentProduct.price}">
                <label>Stock Level</label>
                <input name="stockAmount" required value="${currentProduct.stockAmount}">
                <label>Product image</label>
                <input name="imageURL" type="url" required value="${currentProduct.imageURL}"></label>
                    <button style="width:13rem;">Update Product</button>
            </form>
            </article>
            </section></section></section>
        `
        const modalElUpdateProduct = document.getElementById('updateProductModal')
        modalElUpdateProduct.innerHTML = modalContentUpdateProduct
        
    } else {
        const modalElUpdateProduct = document.getElementById('updateProductModal')
        modalElUpdateProduct.innerHTML = ""
    }
}
function updateProduct(HTMLform){
    const mapping = `/sections/${currentSection.id}/products/${currentProduct.id}`
    const data = new FormData(HTMLform)
    const name = data.get('name')
    const description = data.get('description')
    const price = data.get('price')
    const stockAmount = data.get('stockAmount')
    const imageURL = data.get('imageURL')
    fetch(mapping, {
        method: 'PUT',
        headers: {
            "content-Type": "application/json"
        },
        body: JSON.stringify({name, description, price, stockAmount, imageURL})
    }).then(res => res.json()).then(console.log).catch(console.error)
    confirm()
}
function getUpdateSectionFormHtml(){
    state.updateSection = true
    if (state.updateSection) {
        const modalContentUpdateSection = 
        `
        <section class="modal-bg" >
            <section class="modal-box-add">
                <section class="box-header"><div>Update Product</div><button style="justify-content: right;" onclick="closeUpdateSection()">X</button></section>
                    <section class="additem">
        <article>
            <form class="form" onsubmit="event.preventDefault();updateSection(this);" >
                <label>Section Name</label>
                <input name="name" required value="${currentSection.name}">
                <label>Section image</label>
                <input name="imageURL" type="url" required value="${currentSection.imageURL}"></label>
                    <button style="width:13rem;">Update Section</button>
            </form>
            </article>
            </section></section></section>
        `
        const modalElUpdateSection = document.getElementById('updateSectionModal')
        modalElUpdateSection.innerHTML = modalContentUpdateSection
        
    } else {
        const modalElUpdateSection = document.getElementById('updateSectionModal')
        modalElUpdateSection.innerHTML = ""
    }
}
function updateSection(HTMLform){
    const mapping = `/sections/${currentSection.id}`
    const data = new FormData(HTMLform)
    const name = data.get('name')
    const imageURL = data.get('imageURL')
    fetch(mapping, {
        method: 'PUT',
        headers: {
            "content-Type": "application/json"
        },
        body: JSON.stringify({name, imageURL})
    }).then(res => res.json()).then(console.log).catch(console.error)
    confirm()
}
function deleteSection(){
    const mapping = `/sections/${currentSection.id}`
    
    fetch(mapping, {
        method: 'DELETE', 
    }).then(res => res.json()).then(console.log).catch(console.error)
    confirm()
}
function deleteProduct(){
    const mapping = `/sections/${currentSection.id}/products/${currentProduct.id}`
 
    fetch(mapping, {
        method: 'DELETE', 
    }).then(res => res.json()).then(console.log).catch(console.error)
    confirm()
    
}