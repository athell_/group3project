package com.barclays.group3;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {
    private ProductRepo productRepo;
    private SectionRepo sectionRepo;
    private BasketRepo basketRepo;
    
    public ProductController(ProductRepo productRepo, SectionRepo sectionRepo, BasketRepo basketRepo){
        this.productRepo = productRepo;
        this.sectionRepo = sectionRepo;
        this.basketRepo = basketRepo;
    }



    @GetMapping("/sections/{section_id}/products")
    public List<Product> getProducts(@PathVariable Integer section_id){
        Section section = sectionRepo.findById(section_id).get();
        return section.getProducts();
    }

    @GetMapping("/sections/{section_id}/products/{product_id}")
    public Product getOne(@PathVariable Integer product_id){
        return productRepo.findById(product_id).orElseThrow();
    }

    @PostMapping("/sections/{section_id}/products")
        public Product createProduct(@RequestBody Product productData, @PathVariable Integer section_id){
            Section section = sectionRepo.findById(section_id).get();
            productData.setSection(section);
            return productRepo.save(productData);    
    }

    @PutMapping("/sections/{section_id}/products/{product_id}")
        public Product updateOne(@RequestBody Product updateProduct, @PathVariable Integer product_id){
            return productRepo.findById(product_id).map(product -> {
                product.setDescription(updateProduct.getDescription());
                product.setName(updateProduct.getName());
                product.setImageURL(updateProduct.getImageURL());
                product.setPrice(updateProduct.getPrice());
                product.setStockAmount(updateProduct.getStockAmount());
                return productRepo.save(product);
            }).orElseThrow();
        }

    @DeleteMapping("/sections/{section_id}/products/{product_id}")
    public void deleteOne(@PathVariable Integer product_id){
        productRepo.deleteById(product_id);
    }

    @GetMapping("/baskets/{basket_id}/products")
    public List<Product> getProducts(@PathVariable String basket_id){
        Basket basket = basketRepo.findById(basket_id);
        return basket.getProducts();
    }
    
    @PostMapping("/baskets/{basket_id}/products/{product_id}")
    public Basket addProduct(@PathVariable String basket_id, @PathVariable Integer product_id) {
        Product product = productRepo.findById(product_id).get();
        Basket basket = basketRepo.findById(basket_id);
        basket.addProducts(product);
        return basketRepo.save(basket);
    }

}




