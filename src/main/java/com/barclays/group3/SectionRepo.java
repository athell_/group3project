package com.barclays.group3;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SectionRepo extends JpaRepository<Section, Integer>{
    
}
