package com.barclays.group3;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;


@Entity
public class Basket {
    @Id
    private String id;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {
        CascadeType.DETACH,
        CascadeType.MERGE,
        CascadeType.REFRESH,
        CascadeType.PERSIST
    },
    targetEntity = Product.class)
    @JoinTable(
        name = "ProductsInBasket",
        joinColumns = @JoinColumn(name = "basket_Id"),
        inverseJoinColumns = @JoinColumn(name = "product_id")
    )
    private List<Product> productsInBasket;

    public Basket(String id){
        this.id=id;
    }

    public String getId() {
        return id;
    }

    public void setUserId(String userId) {
        this.id = userId;
    }

    public int getQuantity() {
        return this.productsInBasket.size();
    }

    public double getTotalPrice() {
        return productsInBasket.stream().map(product -> product.getPrice()).reduce(0.0, (runningTotal, price) -> runningTotal + price);
    }

    public List<Product> getProducts() {
        return productsInBasket;
    }

    public void addProducts(Product products) {
        this.productsInBasket.add(products);
    }

    public void removeProduct(int arrayIndex){
        this.productsInBasket.remove(arrayIndex);
    }

    public void removeAllProducts(){
        this.productsInBasket.clear();
    }

}