package com.barclays.group3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class ProductTest {
    @Test
    public void product_is_in_stock(){
        Product tv = new Product("Tv", "is a tv", 9.99, 10, "https://www.currys.co.uk/products/samsung-ue43au7100kxxu-43-smart-4k-ultra-hd-hdr-led-tv-10222296.html");
        assertTrue(tv.isInStock());
    }

    @Test
    public void product_is_in_section(){
        Product tv = new Product("Tv", "is a tv", 9.99, 10, "https://www.currys.co.uk/products/samsung-ue43au7100kxxu-43-smart-4k-ultra-hd-hdr-led-tv-10222296.html");
        Section televisions = new Section("Televisions", "https://www.currys.co.uk/products/samsung-ue43au7100kxxu-43-smart-4k-ultra-hd-hdr-led-tv-10222296.html");
        tv.setSection(televisions);
        assertEquals("Televisions", tv.getSection().getName());
    }
    @Test
    public void stock_amount_is_reduced_on_sale(){
        Product tv = new Product("Tv", "is a tv", 9.99, 10, "https://www.currys.co.uk/products/samsung-ue43au7100kxxu-43-smart-4k-ultra-hd-hdr-led-tv-10222296.html");
        assertEquals(10, tv.getStockAmount());
        tv.itemHasBeenSold();
        assertEquals(9, tv.getStockAmount());

    }
    //Test below is broken, this.basket is null
    // @Test
    // public void add_product_to_basket(){
    //     Product tv = new Product("Tv", "is a tv", 9.99, 10, "https://www.currys.co.uk/products/samsung-ue43au7100kxxu-43-smart-4k-ultra-hd-hdr-led-tv-10222296.html");
    //     Basket basket = new Basket("1");
    //     tv.setBasket(basket);
    //     assertEquals(basket.getProducts(), tv);


    // }
    
    
}
